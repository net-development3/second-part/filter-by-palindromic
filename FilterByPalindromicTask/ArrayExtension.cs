﻿using System;

namespace FilterByPalindromicTask
{
    public static class ArrayExtension
    {
        public static int[] FilterByPalindromic(int[] source)
        {
            if (source == null) 
            {  
                throw new ArgumentNullException(nameof(source)); 
            }

            if (source.Length == 0) 
            {
                throw new ArgumentException(null, nameof(source)); 
            }

            int count = 0;

            for (int i = 0; i < source.Length; i++)
            {
                int number = source[i], numLength = 0;
                while (number > 0)
                {
                    number /= 10;
                    numLength++;
                }

                number = source[i];
                int[] digits = new int[numLength];

                for (int j = numLength - 1; j >= 0; j--)
                {
                    digits[j] = number % 10;
                    number /= 10;
                }

                bool flag = false;

                for (int j = 0; j < digits.Length; j++)
                {
                    if (digits[j] == digits[digits.Length - j - 1])
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                        source[i] = -1;
                        break;
                    }
                }

                if (flag || (source[i] / 10 == 0 && source[i] >= 0))
                {
                    count++;
                }
            }

            int[] result = new int[count];

            for (int i = 0, j = 0; i < source.Length; i++)
            {
                if (source[i] >= 0)
                {
                    result[j] = source[i];
                    j++;
                }
            }

            return result;
        }
    }
}
